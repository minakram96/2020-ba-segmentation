# 


## Repository outline

The structure of the repository should be organized as follows.

```
+-- evaluation
+-- expose
|   +-- citation.bib
|   +-- root.tex
|   +-- final_version.pdf
+-- literature
|   +-- notes
+-- minutes
+-- misc
+-- presentation
|    +-- midterm.pptx
|    +-- final.pptx
+-- README.md
+-- thesis
|   +-- citation.bib
|   +-- root.tex
|   +-- final_version.pdf

```


## Info
- student: Mina Akram Zaher
- type: bachelor's thesis 
