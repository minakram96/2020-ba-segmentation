Journals:
=========
IEEE Transactions on Robotics (T-RO)
International Journal of Robotics Research (IJRR)
IEEE Robotics and Automation Magazine (RAM)
IEEE Robotics and Automation Letters (R-AL)


Conferences:
============
Robotics: Science and Systems Conference (RSS)
IEEE International Conference on Robotics and Automation (ICRA)
IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS)
IEEE-RAS International Conference on Humanoid Robots (Humanoids)
ACM/IEEE International Conference on Human-Robot Interaction (HRI)



Vision Conferences:
===================
IEEE Conference on Computer Vision and Pattern Recognition (CVPR)
IEEE International Conference on Computer Vision (ICCV)
European Conference on Computer Vision (ECCV)

