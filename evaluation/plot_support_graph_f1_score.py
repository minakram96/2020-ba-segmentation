#!/usr/bin/env python3

import pandas as pd
import matplotlib.pyplot as plt

def main():

    df = pd.read_csv('/tmp/support_graph_f1_score.csv')

    plt.figure()
    df.plot()
    plt.legend(loc='best')
    plt.savefig('/tmp/support_graph_f1_score.png')
    plt.show()

if __name__ == '__main__':
    main()
