% Encoding: UTF-8

@STRING{humanoids = {IEEE/RAS International Conference on Humanoid Robots (Humanoids)}}

@article{Teichman2011,
	abstract = {Object recognition is a critical next step for autonomous robots, but a solution to the problem has remained elusive. Prior 3D-sensor-based work largely classifies individual point cloud segments or uses class-specific trackers. In this paper, we take the approach of classifying the tracks of all visible objects. Our new track classification method, based on a mathematically principled method of combining log odds estimators, is fast enough for real time use, is non-specific to object class, and performs well (98.5{\%} accuracy) on the task of classifying correctly-tracked, well-segmented objects into car, pedestrian, bicyclist, and background classes. We evaluate the classifier's performance using the Stanford Track Collection, a new dataset of about 1.3 million labeled point clouds in about 14,000 tracks recorded from an autonomous vehicle research platform. This dataset, which we make publicly available, contains tracks extracted from about one hour of 360-degree, 10Hz depth information recorded both while driving on busy campus streets and parked at busy intersections. {\textcopyright} 2011 IEEE.},
	author = {Teichman, Alex and Levinson, Jesse and Thrun, Sebastian},
	doi = {10.1109/ICRA.2011.5979636},
	isbn = {9781612843865},
	issn = {10504729},
	journal = {Proceedings - IEEE International Conference on Robotics and Automation},
	pages = {4034--4041},
	publisher = {IEEE},
	title = {{Towards 3D object recognition via classification of arbitrary object tracks}},
	year = {2011}
}

@article{Hornung2013,
	abstract = {Three-dimensional models provide a volumetric representation of space which is important for a variety of robotic applications including flying robots and robots that are equipped with manipulators. In this paper, we present an open-source framework to generate volumetric 3D environment models. Our mapping approach is based on octrees and uses probabilistic occupancy estimation. It explicitly represents not only occupied space, but also free and unknown areas. Furthermore, we propose an octree map compression method that keeps the 3D models compact. Our framework is available as an open-source C++ library and has already been successfully applied in several robotics projects. We present a series of experimental results carried out with real robots and on publicly available real-world datasets. The results demonstrate that our approach is able to update the representation efficiently and models the data consistently while keeping the memory requirement at a minimum. {\textcopyright} 2013 Springer Science+Business Media New York.},
	author = {Hornung, Armin and Wurm, Kai M. and Bennewitz, Maren and Stachniss, Cyrill and Burgard, Wolfram},
	doi = {10.1007/s10514-012-9321-0},
	issn = {09295593},
	journal = {Autonomous Robots},
	keywords = {3D,Mapping,Navigation,Probabilistic},
	number = {3},
	pages = {189--206},
	title = {{OctoMap: An efficient probabilistic 3D mapping framework based on octrees}},
	volume = {34},
	year = {2013}
}

@article{Wolf2015,
	abstract = {In this paper, we present an efficient semantic segmentation framework for indoor scenes operating on 3D point clouds. We use the results of a Random Forest Classifier to initialize the unary potentials of a densely interconnected Conditional Random Field, for which we learn the parameters for the pairwise potentials from training data. These potentials capture and model common spatial relations between class labels, which can often be observed in indoor scenes. We evaluate our approach on the popular NYU Depth datasets, for which it achieves superior results compared to the current state of the art. Exploiting parallelization and applying an efficient CRF inference method based on mean field approximation, our framework is able to process full resolution Kinect point clouds in half a second on a regular laptop, more than twice as fast as comparable methods.},
	author = {Wolf, Daniel and Prankl, Johann and Vincze, Markus},
	doi = {10.1109/ICRA.2015.7139875},
	issn = {10504729},
	journal = {Proceedings - IEEE International Conference on Robotics and Automation},
	number = {June},
	pages = {4867--4873},
	title = {{Fast semantic segmentation of 3D point clouds using a dense CRF with learned parameters}},
	volume = {2015-June},
	year = {2015}
}

@article{Grotz2019,
	abstract = {Robots manipulating objects in cluttered scenes require a semantic scene understanding, which describes objects and their relations. Knowledge about physically plausible support relations among objects in such scenes is key for action execution. Due to occlusions, however, support relations often cannot be reliably inferred from a single view only. In this work, we present an active vision system that mitigates occlusion, and explores the scene for object support relations. We extend our previous work in which physically plausible support relations are extracted based on geometric primitives. The active vision system generates view candidates based on existing support relations among the objects, and selects the next best view. We evaluate our approach in simulation, as well as on the humanoid robot ARMAR-6, and show that the active vision system improves the semantic scene model by extracting physically plausible support relations from multiple views.},
	author = {Grotz, Markus and Sippel, David and Asfour, Tamim},
	isbn = {9781538676295},
	journal = {Humanoids},
	title = {{Active Vision for Extraction of Physically Plausible Support Relations}},
	year = {2019}
}

@article{Held2016,
	abstract = {In order to track dynamic objects in a robot's environment, one must first segment the scene into a collection of separate objects. Most real-time robotic vision systems today rely on simple spatial relations to segment the scene into separate objects. However, such methods fail under a variety of realworld situations such as occlusions or crowds of closely-packed objects. We propose a probabilistic 3D segmentation method that combines spatial, temporal, and semantic information to make better-informed decisions about how to segment a scene. We begin with a coarse initial segmentation. We then compute the probability that a given segment should be split into multiple segments or that multiple segments should be merged into a single segment, using spatial, semantic, and temporal cues. Our probabilistic segmentation framework enables us to significantly reduce both undersegmentations and oversegmentations on the KITTI dataset [3, 4, 5] while still running in real-time. By combining spatial, temporal, and semantic information, we are able to create a more robust 3D segmentation system that leads to better overall perception in crowded dynamic environments.},
	author = {Held, David and Guillory, Devin and Rebsamen, Brice and Thrun, Sebastian and Savarese, Silvio},
	doi = {10.15607/rss.2016.xii.024},
	isbn = {9780992374723},
	issn = {2330765X},
	journal = {Robotics: Science and Systems},
	title = {{A probabilistic framework for real-time 3D segmentation using spatial, temporal, and semantic cues}},
	volume = {12},
	year = {2016}
}

@article{Kartmann2018,
	abstract = {Reliable execution of robot manipulation actions in cluttered environments requires that the robot is able to understand relations between objects and reason about consequences of actions applied to these objects. We present an approach for extracting physically plausible support relations between objects based on visual information, which does not require any prior knowledge about physical object properties, e.g., mass distribution or friction coefficients. Based on a scene representation enriched by such physically plausible support relations between objects, we derive predictions about action effects. These predictions take into account uncertainty about support relations and allow applying strategies for safe bimanual object manipulation when needed. The extraction of physically plausible support relations is evaluated both in simulation and in real-world experiments using real data from a depth camera, whereas the handling of support relation uncertainties is validated on the humanoid robot ARMAR-III.},
	author = {Kartmann, Rainer and Paus, Fabian and Grotz, Markus and Asfour, Tamim},
	doi = {10.1109/LRA.2018.2859448},
	issn = {23773766},
	journal = {IEEE Robotics and Automation Letters},
	keywords = {Perception for grasping and manipulation,RGB-D perception,semantic scene understanding},
	number = {4},
	pages = {3991--3998},
	title = {{Extraction of Physically Plausible Support Relations to Predict and Validate Manipulation Action Effects}},
	volume = {3},
	year = {2018}
}

@article{Mojtahedzadeh2015,
 author = {Mojtahedzadeh, Rasoul and Bouguerra, Abdelbaki and Schaffernicht, Erik and Lilienthal, Achim J.},
 year = {2015},
 title = {Support relation analysis and decision making for safe robotic manipulation tasks},
 pages = {99--117},
 volume = {71},
 issn = {09218890},
 journal = {Robotics and Autonomous Systems},
 doi = {\url{10.1016/j.robot.2014.12.014}}
}

@incollection{Silberman2012,
 author = {Silberman, Nathan and Hoiem, Derek and Kohli, Pushmeet and Fergus, Rob},
 title = {Indoor Segmentation and Support Inference from RGBD Images},
 pages = {746--760},
 volume = {7576},
 publisher = {{Springer Berlin Heidelberg}},
 isbn = {978-3-642-33714-7},
 series = {Lecture Notes in Computer Science},
 editor = {Hutchison, David and Kanade, Takeo and Kittler, Josef and Kleinberg, Jon M. and Mattern, Friedemann and Mitchell, John C. and Naor, Moni and Nierstrasz, Oscar and {Pandu Rangan}, C. and Steffen, Bernhard and Sudan, Madhu and Terzopoulos, Demetri and Tygar, Doug and Vardi, Moshe Y. and Weikum, Gerhard and Fitzgibbon, Andrew and Lazebnik, Svetlana and Perona, Pietro and Sato, Yoichi and Schmid, Cordelia},
 booktitle = {Computer Vision -- ECCV 2012},
 year = {2012},
 address = {Berlin, Heidelberg},
 doi = {\url{10.1007/978-3-642-33715-4{\textunderscore }54}}
}


