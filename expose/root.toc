\babel@toc {english}{}
\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Motivation}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}Problem description}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Scope}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Related work}{4}{chapter.3}
\contentsline {section}{\numberline {3.1}Extraction of Physically Plausible Support Relations to Predict and Validate Manipulation Action Effects (\cite {Kartmann2018})}{4}{section.3.1}
\contentsline {section}{\numberline {3.2}Active Vision for Extraction of Physically Plausible Support Relations (\cite {Grotz2019})}{4}{section.3.2}
\contentsline {section}{\numberline {3.3}Fast Semantic Segmentation of 3D Point Clouds using a Dense CRF with Learned Parameters (\cite {Wolf2015})}{4}{section.3.3}
\contentsline {section}{\numberline {3.4}A probabilistic framework for real-time 3D segmentation using spatial, temporal, and semantic cues (\cite {Held2016})}{4}{section.3.4}
\contentsline {chapter}{\numberline {4}Timeline}{6}{chapter.4}
