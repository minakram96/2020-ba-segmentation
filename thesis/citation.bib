% Encoding: UTF-8

@STRING{humanoids = {IEEE/RAS International Conference on Humanoid Robots (Humanoids)}}

@article{Teichman2011,
	abstract = {Object recognition is a critical next step for autonomous robots, but a solution to the problem has remained elusive. Prior 3D-sensor-based work largely classifies individual point cloud segments or uses class-specific trackers. In this paper, we take the approach of classifying the tracks of all visible objects. Our new track classification method, based on a mathematically principled method of combining log odds estimators, is fast enough for real time use, is non-specific to object class, and performs well (98.5{\%} accuracy) on the task of classifying correctly-tracked, well-segmented objects into car, pedestrian, bicyclist, and background classes. We evaluate the classifier's performance using the Stanford Track Collection, a new dataset of about 1.3 million labeled point clouds in about 14,000 tracks recorded from an autonomous vehicle research platform. This dataset, which we make publicly available, contains tracks extracted from about one hour of 360-degree, 10Hz depth information recorded both while driving on busy campus streets and parked at busy intersections. {\textcopyright} 2011 IEEE.},
	author = {Teichman, Alex and Levinson, Jesse and Thrun, Sebastian},
	doi = {10.1109/ICRA.2011.5979636},
	isbn = {9781612843865},
	issn = {10504729},
	journal = {Proceedings - IEEE International Conference on Robotics and Automation},
	pages = {4034--4041},
	publisher = {IEEE},
	title = {{Towards 3D object recognition via classification of arbitrary object tracks}},
	year = {2011}
}

@article{Hornung2013,
	abstract = {Three-dimensional models provide a volumetric representation of space which is important for a variety of robotic applications including flying robots and robots that are equipped with manipulators. In this paper, we present an open-source framework to generate volumetric 3D environment models. Our mapping approach is based on octrees and uses probabilistic occupancy estimation. It explicitly represents not only occupied space, but also free and unknown areas. Furthermore, we propose an octree map compression method that keeps the 3D models compact. Our framework is available as an open-source C++ library and has already been successfully applied in several robotics projects. We present a series of experimental results carried out with real robots and on publicly available real-world datasets. The results demonstrate that our approach is able to update the representation efficiently and models the data consistently while keeping the memory requirement at a minimum. {\textcopyright} 2013 Springer Science+Business Media New York.},
	author = {Hornung, Armin and Wurm, Kai M. and Bennewitz, Maren and Stachniss, Cyrill and Burgard, Wolfram},
	doi = {10.1007/s10514-012-9321-0},
	issn = {09295593},
	journal = {Autonomous Robots},
	keywords = {3D,Mapping,Navigation,Probabilistic},
	number = {3},
	pages = {189--206},
	title = {{OctoMap: An efficient probabilistic 3D mapping framework based on octrees}},
	volume = {34},
	year = {2013}
}

@article{Wolf2015,
	abstract = {In this paper, we present an efficient semantic segmentation framework for indoor scenes operating on 3D point clouds. We use the results of a Random Forest Classifier to initialize the unary potentials of a densely interconnected Conditional Random Field, for which we learn the parameters for the pairwise potentials from training data. These potentials capture and model common spatial relations between class labels, which can often be observed in indoor scenes. We evaluate our approach on the popular NYU Depth datasets, for which it achieves superior results compared to the current state of the art. Exploiting parallelization and applying an efficient CRF inference method based on mean field approximation, our framework is able to process full resolution Kinect point clouds in half a second on a regular laptop, more than twice as fast as comparable methods.},
	author = {Wolf, Daniel and Prankl, Johann and Vincze, Markus},
	doi = {10.1109/ICRA.2015.7139875},
	issn = {10504729},
	journal = {Proceedings - IEEE International Conference on Robotics and Automation},
	number = {June},
	pages = {4867--4873},
	title = {{Fast semantic segmentation of 3D point clouds using a dense CRF with learned parameters}},
	volume = {2015-June},
	year = {2015}
}

@article{Grotz2019,
	abstract = {Robots manipulating objects in cluttered scenes require a semantic scene understanding, which describes objects and their relations. Knowledge about physically plausible support relations among objects in such scenes is key for action execution. Due to occlusions, however, support relations often cannot be reliably inferred from a single view only. In this work, we present an active vision system that mitigates occlusion, and explores the scene for object support relations. We extend our previous work in which physically plausible support relations are extracted based on geometric primitives. The active vision system generates view candidates based on existing support relations among the objects, and selects the next best view. We evaluate our approach in simulation, as well as on the humanoid robot ARMAR-6, and show that the active vision system improves the semantic scene model by extracting physically plausible support relations from multiple views.},
	author = {Grotz, Markus and Sippel, David and Asfour, Tamim},
	isbn = {9781538676295},
	journal = {Humanoids},
	title = {{Active Vision for Extraction of Physically Plausible Support Relations}},
	year = {2019}
}

@article{Held2016,
	abstract = {In order to track dynamic objects in a robot's environment, one must first segment the scene into a collection of separate objects. Most real-time robotic vision systems today rely on simple spatial relations to segment the scene into separate objects. However, such methods fail under a variety of realworld situations such as occlusions or crowds of closely-packed objects. We propose a probabilistic 3D segmentation method that combines spatial, temporal, and semantic information to make better-informed decisions about how to segment a scene. We begin with a coarse initial segmentation. We then compute the probability that a given segment should be split into multiple segments or that multiple segments should be merged into a single segment, using spatial, semantic, and temporal cues. Our probabilistic segmentation framework enables us to significantly reduce both undersegmentations and oversegmentations on the KITTI dataset [3, 4, 5] while still running in real-time. By combining spatial, temporal, and semantic information, we are able to create a more robust 3D segmentation system that leads to better overall perception in crowded dynamic environments.},
	author = {Held, David and Guillory, Devin and Rebsamen, Brice and Thrun, Sebastian and Savarese, Silvio},
	doi = {10.15607/rss.2016.xii.024},
	isbn = {9780992374723},
	issn = {2330765X},
	journal = {Robotics: Science and Systems},
	title = {{A probabilistic framework for real-time 3D segmentation using spatial, temporal, and semantic cues}},
	volume = {12},
	year = {2016}
}

@article{Kartmann2018,
	abstract = {Reliable execution of robot manipulation actions in cluttered environments requires that the robot is able to understand relations between objects and reason about consequences of actions applied to these objects. We present an approach for extracting physically plausible support relations between objects based on visual information, which does not require any prior knowledge about physical object properties, e.g., mass distribution or friction coefficients. Based on a scene representation enriched by such physically plausible support relations between objects, we derive predictions about action effects. These predictions take into account uncertainty about support relations and allow applying strategies for safe bimanual object manipulation when needed. The extraction of physically plausible support relations is evaluated both in simulation and in real-world experiments using real data from a depth camera, whereas the handling of support relation uncertainties is validated on the humanoid robot ARMAR-III.},
	author = {Kartmann, Rainer and Paus, Fabian and Grotz, Markus and Asfour, Tamim},
	doi = {10.1109/LRA.2018.2859448},
	issn = {23773766},
	journal = {IEEE Robotics and Automation Letters},
	keywords = {Perception for grasping and manipulation,RGB-D perception,semantic scene understanding},
	number = {4},
	pages = {3991--3998},
	title = {{Extraction of Physically Plausible Support Relations to Predict and Validate Manipulation Action Effects}},
	volume = {3},
	year = {2018}
}

@incollection{Silberman2012,
	author = {Silberman, Nathan and Hoiem, Derek and Kohli, Pushmeet and Fergus, Rob},
	title = {Indoor Segmentation and Support Inference from RGBD Images},
	pages = {746--760},
	volume = {7576},
	publisher = {{Springer Berlin Heidelberg}},
	isbn = {978-3-642-33714-7},
	series = {Lecture Notes in Computer Science},
	editor = {Hutchison, David and Kanade, Takeo and Kittler, Josef and Kleinberg, Jon M. and Mattern, Friedemann and Mitchell, John C. and Naor, Moni and Nierstrasz, Oscar and {Pandu Rangan}, C. and Steffen, Bernhard and Sudan, Madhu and Terzopoulos, Demetri and Tygar, Doug and Vardi, Moshe Y. and Weikum, Gerhard and Fitzgibbon, Andrew and Lazebnik, Svetlana and Perona, Pietro and Sato, Yoichi and Schmid, Cordelia},
	booktitle = {Computer Vision -- ECCV 2012},
	year = {2012},
	address = {Berlin, Heidelberg},
	doi = {\url{10.1007/978-3-642-33715-4{\textunderscore }54}}
}

@article{Vahrenkamp2015,
	abstract = {With ArmarX we introduce a robot programming environment that has been developed in order to ease the realization of higher level capabilities needed by complex robotic systems such as humanoid robots. ArmarX is built upon the idea that consistent disclosure of the system state strongly facilitates the development process of distributed robot applications. We show the applicability of ArmarX by introducing a robot architecture for a humanoid system and discuss essential aspects based on an exemplary pick and place task. With several tools that are provided by the ArmarX framework, such as graphical user interfaces (GUI) or statechart editors, the programmer is enabled to efficiently build and inspect component based robotics software systems.},
	author = {Vahrenkamp, Nikolaus and W{\"{a}}chter, Mirko and Kr{\"{o}}hnert, Manfred and Welke, Kai and Asfour, Tamim},
	doi = {10.1515/itit-2014-1066},
	file = {:D\:/OneDrive - student.kit.edu/Uni Documents/Bachelor Thesis/literature/2015-The_Robot_Software_Framework_ArmarX.pdf:pdf},
	issn = {1611-2776},
	journal = {it - Information Technology},
	keywords = {component-based software development,robot development environment,robotics software framework,software for humanoid robots},
	number = {2},
	pages = {1--10},
	title = {{The robot software framework ArmarX}},
	volume = {57},
	year = {2015}
}

@inproceedings{Asfour2006,
	address = {Genova, Italy},
	author = {Asfour, T and Regenstein, K and Azad, P and Schr{\"{o}}der, J and Vahrenkamp, N and Dillmann, R},
	booktitle = {IEEE/RAS International Conference on Humanoid Robots (Humanoids)},
	month = {dec},
	pages = {169--175},
	title = {{ARMAR-III: An Integrated Humanoid Platform for Sensory-Motor Control}},
	year = {2006}
}

@article{Asfour2019,
	abstract = {We present the collaborative humanoid robot ARMAR-6, which has been developed to perform a wide variety of complex maintenance tasks in industrial environments, collaborating with human workers. We present the hardware, software, and functional architecture of the robot as well as its current abilities. Those include the recognition of the need of help of a human worker, the execution of maintenance plans, compliant bimanual manipulation, vision-based grasping, fluent object handover, human activity recognition, natural dialog, navigation and more. We demonstrate the high level of technology readiness for real world applications in a complex demonstration scenario, shown more than 50 times at the CEBIT 2018 exhibition.},
	author = {Asfour, Tamim and Kaul, Lukas and Wachter, Mirko and Ottenhaus, Simon and Weiner, Pascal and Rader, Samuel and Grimm, Raphael and Zhou, You and Grotz, Markus and Paus, Fabian and Shingarey, Dmitriy and Haubert, Hans},
	doi = {10.1109/HUMANOIDS.2018.8624966},
	file = {:D\:/OneDrive - student.kit.edu/Uni Documents/Bachelor Thesis/literature/2016-ARMAR6.pdf:pdf},
	isbn = {9781538672839},
	issn = {21640580},
	journal = {IEEE-RAS International Conference on Humanoid Robots},
	pages = {447--454},
	title = {{ARMAR-6: A Collaborative Humanoid Robot for Industrial Environments}},
	volume = {2018-Novem},
	year = {2019}
}

@article{Asfour2015,
	abstract = {We present the mechatronic design of the next generation of our humanoid robots, the humanoid robot ARMAR-4, a full body torque controlled humanoid robot with 63 active degrees of freedom, 63 actuators, 214 sensors, 76 microcontroller for low-level control, 3 PCs for perception, high-level control and balancing, a weight of 70 kg including batteries and total height of 170 cm. In designing the robot we follow an integrated approach towards the implementation of high performance humanoid robot systems, able to act and interact in the real world using only on-board sensors and computation power. Special attention was paid to the realization of advanced bimanual manipulation and locomotion capabilities. The paper presents the design concept of the robot and its mechatronic realization.},
	author = {Asfour, Tamim and Schill, Julian and Peters, Heiner and Klas, Cornelius and Bucker, Jens and Sander, Christian and Schulz, Stefan and Kargov, Artem and Werner, Tino and Bartenbach, Volker},
	doi = {10.1109/HUMANOIDS.2013.7030004},
	file = {:D\:/OneDrive - student.kit.edu/Uni Documents/Bachelor Thesis/literature/2015-Armar4.pdf:pdf},
	isbn = {9781479926176},
	issn = {21640580},
	journal = {IEEE-RAS International Conference on Humanoid Robots},
	number = {February},
	pages = {390--396},
	title = {{ARMAR-4: A 63 DOF torque controlled humanoid robot}},
	volume = {2015-Febru},
	year = {2015}
}

@article{Muja2009,
	abstract = {For many computer vision problems, the most time consuming component consists of nearest neighbor matching in high-dimensional spaces. There are no known exact algorithms for solving these high-dimensional problems that are faster than linear search. Approximate algorithms are known to provide large speedups with only minor loss in accuracy, but many such algorithms have been published with only minimal guidance on selecting an algorithm and its parameters for any given problem. In this paper, we describe a system that answers the question, "What is the fastest approximate nearest-neighbor algorithm for my data?" Our system will take any given dataset and desired degree of precision and use these to automatically determine the best algorithm and parameter values. We also describe a new algorithm that applies priority search on hierarchical k-means trees, which we have found to provide the best known performance on many datasets. After testing a range of alternatives, we have found that multiple randomized k-d trees provide the best performance for other datasets. We are releasing public domain code that implements these approaches. This library provides about one order of magnitude improvement in query time over the best previously available software and provides fully automated parameter selection.},
	author = {Muja, Marius and Lowe, David G.},
	file = {:D\:/OneDrive - student.kit.edu/Uni Documents/Bachelor Thesis/literature/2009-flann.pdf:pdf},
	isbn = {9789898111692},
	journal = {VISAPP 2009 - Proceedings of the 4th International Conference on Computer Vision Theory and Applications},
	keywords = {Clustering,Hierarchical k-means tree,Nearest-neighbors search,Randomized kd-trees},
	pages = {331--340},
	title = {{Fast approximate nearest neighbors with automatic algorithm configuration}},
	url = {https://lear.inrialpes.fr/$\sim$douze/enseignement/2014-2015/presentation_papers/muja_flann.pdf},
	volume = {1},
	year = {2009}
}

@article{Rusu2011,
	abstract = {With the advent of new, low-cost 3D sensing hardware such as the Kinect, and continued efforts in advanced point cloud processing, 3D perception gains more and more importance in robotics, as well as other fields. In this paper we present one of our most recent initiatives in the areas of point cloud perception: PCL (Point Cloud Library - http://pointclouds.org). PCL presents an advanced and extensive approach to the subject of 3D perception, and it's meant to provide support for all the common 3D building blocks that applications need. The library contains state-of-the art algorithms for: filtering, feature estimation, surface reconstruction, registration, model fitting and segmentation. PCL is supported by an international community of robotics and perception researchers. We provide a brief walkthrough of PCL including its algorithmic capabilities and implementation strategies. {\textcopyright} 2011 IEEE.},
	author = {Rusu, Radu Bogdan and Cousins, Steve},
	doi = {10.1109/ICRA.2011.5980567},
	file = {:D\:/OneDrive - student.kit.edu/Uni Documents/Bachelor Thesis/literature/2011-PCL.pdf:pdf},
	isbn = {9781612843865},
	issn = {10504729},
	journal = {Proceedings - IEEE International Conference on Robotics and Automation},
	pages = {1--4},
	publisher = {IEEE},
	title = {{3D is here: Point Cloud Library (PCL)}},
	year = {2011}
}

@article{Henning2004,
	author = {Henning, Michi},
	file = {:D\:/OneDrive - student.kit.edu/Uni Documents/Bachelor Thesis/literature/2004-ICE.pdf:pdf},
	number = {February},
	pages = {66--75},
	title = {{A New Approach to Object-Oriented Middleware}},
	year = {2004}
}

@book{Beyerer2015,
	abstract = {The book offers a thorough introduction to machine vision. It is organized in two parts. The first part covers the image acquisition, which is the crucial component of most automated visual inspection systems. All important methods are described in great detail and are presented with a reasoned structure. The second part deals with the modeling and processing of image signals and pays particular regard to methods, which are relevant for automated visual inspection.},
	author = {Beyerer, J{\"{u}}rgen and Le{\'{o}}n, Fernando Puente and Frese, Christian},
	booktitle = {Machine Vision: Automated Visual Inspection: Theory, Practice and Applications},
	doi = {10.1007/978-3-662-47794-6},
	file = {:D\:/OneDrive - student.kit.edu/Uni Documents/Bachelor Thesis/literature/2016_Book_MachineVision.pdf:pdf},
	isbn = {9783662477946},
	pages = {1--798},
	title = {{Machine vision: Automated visual inspection: Theory, practice and applications}},
	year = {2015}
}

@article{Schnabel2007,
	abstract = {In this paper we present an automatic algorithm to detect basic shapes in unorganized point clouds. The algorithm decomposes the point cloud into a concise, hybrid structure of inherent shapes and a set of remaining points. Each detected shape serves as a proxy for a set of corresponding points. Our method is based on random sampling and detects planes, spheres, cylinders, cones and tori. For models with surfaces composed of these basic shapes only, for example, CAD models, we automatically obtain a representation solely consisting of shape proxies. We demonstrate that the algorithm is robust even in the presence of many outliers and a high degree of noise. The proposed method scales well with respect to the size of the input point cloud and the number and size of the shapes within the data. Even point sets with several millions of samples are robustly decomposed within less than a minute. Moreover, the algorithm is conceptually simple and easy to implement. Application areas include measurement of physical parameters, scan registration, surface compression, hybrid rendering, shape classification, meshing, simplification, approximation and reverse engineering. {\textcopyright} 2007 The Eurographics Association and Blackwell Publishing Ltd.},
	author = {Schnabel, R. and Wahl, R. and Klein, R.},
	doi = {10.1111/j.1467-8659.2007.01016.x},
	file = {:D\:/OneDrive - student.kit.edu/Uni Documents/Bachelor Thesis/literature/2007-RANSAC.pdf:pdf},
	issn = {14678659},
	journal = {Computer Graphics Forum},
	keywords = {Geometry analysis,Large point-clouds,Localized RANSAC,Primitive shapes,Shape fitting},
	number = {2},
	pages = {214--226},
	title = {{Efficient RANSAC for point-cloud shape detection}},
	volume = {26},
	year = {2007}
}

@article{Mojtahedzadeh2015,
	abstract = {In this article, we describe an approach to address the issue of automatically building and using high-level symbolic representations that capture physical interactions between objects in static configurations. Our work targets robotic manipulation systems where objects need to be safely removed from piles that come in random configurations. We assume that a 3D visual perception module exists so that objects in the piles can be completely or partially detected. Depending on the outcome of the perception, we divide the issue into two sub-issues: (1) all objects in the configuration are detected; (2) only a subset of objects are correctly detected. For the first case, we use notions from geometry and static equilibrium in classical mechanics to automatically analyze and extract act and support relations between pairs of objects. For the second case, we use machine learning techniques to estimate the probability of objects supporting each other. Having the support relations extracted, a decision making process is used to identify which object to remove from the configuration so that an expected minimum cost is optimized. The proposed methods have been extensively tested and validated on data sets generated in simulation and from real world configurations for the scenario of unloading goods from shipping containers.},
	author = {Mojtahedzadeh, Rasoul and Bouguerra, Abdelbaki and Schaffernicht, Erik and Lilienthal, Achim J.},
	doi = {10.1016/j.robot.2014.12.014},
	file = {:D\:/OneDrive - student.kit.edu/Uni Documents/Bachelor Thesis/literature/2015-support_relations.pdf:pdf},
	issn = {09218890},
	journal = {Robotics and Autonomous Systems},
	keywords = {Decision making,Machine learning,Robotic manipulation,Scene analysis,World models},
	pages = {99--117},
	publisher = {Elsevier B.V.},
	title = {{Support relation analysis and decision making for safe robotic manipulation tasks}},
	url = {http://dx.doi.org/10.1016/j.robot.2014.12.014},
	volume = {71},
	year = {2015}
}

@article{Ottenhaus2019,
	abstract = {Grasping unknown objects is a challenging task for humanoid robots, as planning and execution have to cope with noisy sensor data. This work presents a framework, which integrates sensing, planning and acting in one visuo-haptic grasping pipeline. Visual and tactile perception are fused using Gaussian Process Implicit Surfaces to estimate the object surface. Two grasp planners then generate grasp candidates, which are used to train a neural network to determine the best grasp. The main contribution of this work is the introduction of a discriminative deep neural network for scoring grasp hypotheses for underactuated humanoid hands. The pipeline delivers full 6D grasp poses for multi-fingered humanoid hands but it is not limited to any specific gripper. The pipeline is trained and evaluated in simulation, based on objects from the YCB and KIT object sets, resulting in a 95 % success rate regarding force-closure. To prove the validity of the proposed approach, the pipeline is executed on the humanoid robot ARMAR-6 in experiments with eight non-Trivial objects using an underactuated five finger hand.},
	author = {Ottenhaus, Simon and Renninghoff, Daniel and Grimm, Raphael and Ferreira, Fabio and Asfour, Tamim},
	doi = {10.1109/Humanoids43949.2019.9035002},
	file = {:D\:/OneDrive - student.kit.edu/Uni Documents/Bachelor Thesis/literature/2019-grasping.pdf:pdf},
	isbn = {9781538676301},
	issn = {21640580},
	journal = {IEEE-RAS International Conference on Humanoid Robots},
	pages = {402--408},
	title = {{Visuo-haptic grasping of unknown objects based on Gaussian process implicit surfaces and deep learning}},
	volume = {2019-Octob},
	year = {2019}
}

@article{Kendall2017,
	abstract = {There are two major types of uncertainty one can model. Aleatoric uncertainty captures noise inherent in the observations. On the other hand, epistemic uncertainty accounts for uncertainty in the model - uncertainty which can be explained away given enough data. Traditionally it has been difficult to model epistemic uncertainty in computer vision, but with new Bayesian deep learning tools this is now possible. We study the benefits of modeling epistemic vs. aleatoric uncertainty in Bayesian deep learning models for vision tasks. For this we present a Bayesian deep learning framework combining input-dependent aleatoric uncertainty together with epistemic uncertainty. We study models under the framework with per-pixel semantic segmentation and depth regression tasks. Further, our explicit uncertainty formulation leads to new loss functions for these tasks, which can be interpreted as learned attenuation. This makes the loss more robust to noisy data, also giving new state-of-the-art results on segmentation and depth regression benchmarks.},
	archivePrefix = {arXiv},
	arxivId = {arXiv:1703.04977v2},
	author = {Kendall, Alex and Gal, Yarin},
	eprint = {arXiv:1703.04977v2},
	file = {:D\:/OneDrive - student.kit.edu/Uni Documents/Bachelor Thesis/literature/2017-What_Uncertainties_Do_We_Need_in_Bayesian_Deep_Learning_for_Computer_Vision.pdf:pdf},
	issn = {10495258},
	journal = {Advances in Neural Information Processing Systems},
	number = {Nips},
	pages = {5575--5585},
	title = {{What uncertainties do we need in Bayesian deep learning for computer vision?}},
	volume = {2017-Decem},
	year = {2017}
}

@article{Kiureghian2009,
	abstract = {The sources and characters of uncertainties in engineering modeling for risk and reliability analyses are discussed. While many sources of uncertainty may exist, they are generally categorized as either aleatory or epistemic. Uncertainties are characterized as epistemic, if the modeler sees a possibility to reduce them by gathering more data or by refining models. Uncertainties are categorized as aleatory if the modeler does not foresee the possibility of reducing them. From a pragmatic standpoint, it is useful to thus categorize the uncertainties within a model, since it then becomes clear as to which uncertainties have the potential of being reduced. More importantly, epistemic uncertainties may introduce dependence among random events, which may not be properly noted if the character of uncertainties is not correctly modeled. Influences of the two types of uncertainties in reliability assessment, codified design, performance-based engineering and risk-based decision-making are discussed. Two simple examples demonstrate the influence of statistical dependence arising from epistemic uncertainties on systems and time-variant reliability problems. {\textcopyright} 2008 Elsevier Ltd. All rights reserved.},
	author = {Kiureghian, Armen Der and Ditlevsen, Ove},
	doi = {10.1016/j.strusafe.2008.06.020},
	file = {:D\:/OneDrive - student.kit.edu/Uni Documents/Bachelor Thesis/literature/2008-aleatoricVSepistemic.pdf:pdf},
	issn = {01674730},
	journal = {Structural Safety},
	keywords = {Aleatory,Epistemic,Ergodicity,Parameter uncertainty,Predictive models,Probability distribution choice,Statistical dependence,Systems,Time-variant reliability,Uncertainty},
	number = {2},
	pages = {105--112},
	publisher = {Elsevier Ltd},
	title = {{Aleatory or epistemic? Does it matter?}},
	url = {http://dx.doi.org/10.1016/j.strusafe.2008.06.020},
	volume = {31},
	year = {2009}
}

@article{Krahenbuhl2013,
	abstract = {Dense random fields are models in which all pairs of variables are directly connected by pairwise potentials. It has recently been shown that mean field inference in dense random fields can be performed efficiently and that these models enable significant accuracy gains in computer vision applications. However, parameter estimation for dense random fields is still poorly understood. In this paper, we present an efficient algorithm for learning parameters in dense random fields. All parameters are estimated jointly, thus capturing dependencies between them. We show that gradients of a variety of loss functions over the mean field marginals can be computed efficiently. The resulting algorithm learns parameters that directly optimize the performance of mean field inference in the model. As a supporting result, we present an efficient inference algorithm for dense random fields that is guaranteed to converge. Copyright 2013 by the author(s).},
	author = {Kr{\"{a}}henb{\"{u}}hl, Philipp and Koltun, Vladlen},
	file = {:D\:/OneDrive - student.kit.edu/Uni Documents/Bachelor Thesis/literature/2013-DenseCRF.pdf:pdf},
	journal = {30th International Conference on Machine Learning, ICML 2013},
	number = {PART 2},
	pages = {1550--1558},
	title = {{Parameter learning and convergent inference for dense random fields}},
	volume = {28},
	year = {2013}
}

@article{Corke2020,
	abstract = {The computer vision and robotics research communities are each strong. However progress in computer vision has become turbo-charged in recent years due to big data, GPU computing, novel learning algorithms and a very effective research methodology. By comparison, progress in robotics seems slower. It is true that robotics came later to exploring the potential of learning -- the advantages over the well-established body of knowledge in dynamics, kinematics, planning and control is still being debated, although reinforcement learning seems to offer real potential. However, the rapid development of computer vision compared to robotics cannot be only attributed to the former's adoption of deep learning. In this paper, we argue that the gains in computer vision are due to research methodology -- evaluation under strict constraints versus experiments; bold numbers versus videos.},
	archivePrefix = {arXiv},
	arxivId = {2001.02366},
	author = {Corke, Peter and Dayoub, Feras and Hall, David and Skinner, John and S{\"{u}}nderhauf, Niko},
	eprint = {2001.02366},
	file = {:home/minakram96/.local/share/data/Mendeley Ltd./Mendeley Desktop/Downloaded/Corke et al. - 2020 - What can robotics research learn from computer vision research.pdf:pdf},
	title = {{What can robotics research learn from computer vision research?}},
	url = {http://arxiv.org/abs/2001.02366},
	year = {2020}
}

@article{Schwarz2018,
	abstract = {Autonomous robotic manipulation in clutter is challenging. A large variety of objects must be perceived in complex scenes, where they are partially occluded and embedded among many distractors, often in restricted spaces. To tackle these challenges, we developed a deep-learning approach that combines object detection and semantic segmentation. The manipulation scenes are captured with RGB-D cameras, for which we developed a depth fusion method. Employing pretrained features makes learning from small annotated robotic datasets possible. We evaluate our approach on two challenging datasets: one captured for the Amazon Picking Challenge 2016, where our team NimbRo came in second in the Stowing and third in the Picking task; and one captured in disaster-response scenarios. The experiments show that object detection and semantic segmentation complement each other and can be combined to yield reliable object perception.},
	annote = {Uses Cnn and SVM to segment images. Uses depth information to better improve the ability to detect features.
	
	Presented very accurate segmentation, and success rates, but for the purpose of scene understanding, no support graphs are evaluated, resulting in inability to consider things falling when an objects is grasped in the scene. 
	
	Also has the issue of being very finely tuned for the disaster-response environment.},
	author = {Schwarz, Max and Milan, Anton and Periyasamy, Arul Selvam and Behnke, Sven},
	doi = {10.1177/0278364917713117},
	file = {:home/minakram96/Desktop/BachelorThesis/literature/2017-RGB-D object detection .pdf:pdf},
	issn = {17413176},
	journal = {International Journal of Robotics Research},
	keywords = {Deep learning,RGB-D camera,object detection,object perception,semantic segmentation,transfer learning},
	number = {4-5},
	pages = {437--451},
	title = {{RGB-D object detection and semantic segmentation for autonomous manipulation in clutter}},
	volume = {37},
	year = {2018}
}

@article{Rosu2019,
	abstract = {Scene understanding is an important capability for robots acting in unstructured environments. While most SLAM approaches provide a geometrical representation of the scene, a semantic map is necessary for more complex interactions with the surroundings. Current methods treat the semantic map as part of the geometry which limits scalability and accuracy. We propose to represent the semantic map as a geometrical mesh and a semantic texture coupled at independent resolution. The key idea is that in many environments the geometry can be greatly simplified without loosing fidelity, while semantic information can be stored at a higher resolution, independent of the mesh. We construct a mesh from depth sensors to represent the scene geometry and fuse information into the semantic texture from segmentations of individual RGB views of the scene. Making the semantics persistent in a global mesh enables us to enforce temporal and spatial consistency of the individual view predictions. For this, we propose an efficient method of establishing consensus between individual segmentations by iteratively retraining semantic segmentation with the information stored within the map and using the retrained segmentation to re-fuse the semantics. We demonstrate the accuracy and scalability of our approach by reconstructing semantic maps of scenes from NYUv2 and a scene spanning large buildings.},
	annote = {Using meshes as part of the training and labeling information
	
	Used NYUv2 for evaluation},
	archivePrefix = {arXiv},
	arxivId = {arXiv:1906.07029v1},
	author = {Rosu, Radu Alexandru and Quenzel, Jan and Behnke, Sven},
	doi = {10.1007/s11263-019-01187-z},
	eprint = {arXiv:1906.07029v1},
	file = {:home/minakram96/Desktop/BachelorThesis/literature/2019-semi-supervised semantic.pdf:pdf},
	isbn = {1126301901187},
	issn = {15731405},
	journal = {International Journal of Computer Vision},
	keywords = {Label propagation,Semantic mapping,Semantic textured mesh},
	title = {{Semi-supervised Semantic Mapping Through Label Propagation with Semantic Texture Meshes}},
	year = {2019}
}

@article{Garcia-Garcia2017,
	abstract = {Image semantic segmentation is more and more being of interest for computer vision and machine learning researchers. Many applications on the rise need accurate and efficient segmentation mechanisms: autonomous driving, indoor navigation, and even virtual or augmented reality systems to name a few. This demand coincides with the rise of deep learning approaches in almost every field or application target related to computer vision, including semantic segmentation or scene understanding. This paper provides a review on deep learning methods for semantic segmentation applied to various application areas. Firstly, we describe the terminology of this field as well as mandatory background concepts. Next, the main datasets and challenges are exposed to help researchers decide which are the ones that best suit their needs and their targets. Then, existing methods are reviewed, highlighting their contributions and their significance in the field. Finally, quantitative results are given for the described methods and the datasets in which they were evaluated, following up with a discussion of the results. At last, we point out a set of promising future works and draw our own conclusions about the state of the art of semantic segmentation using deep learning techniques.},
	archivePrefix = {arXiv},
	arxivId = {1704.06857},
	author = {Garcia-Garcia, Alberto and Orts-Escolano, Sergio and Oprea, Sergiu and Villena-Martinez, Victor and Garcia-Rodriguez, Jose},
	eprint = {1704.06857},
	file = {:home/minakram96/Desktop/BachelorThesis/literature/2017-review{\_}on{\_}deep{\_}learning.pdf:pdf},
	pages = {1--23},
	title = {{A Review on Deep Learning Techniques Applied to Semantic Segmentation}},
	url = {http://arxiv.org/abs/1704.06857},
	year = {2017}
}

@article{Krahenbuhl2011,
	abstract = {Most state-of-the-art techniques for multi-class image segmentation and labeling use conditional random fields defined over pixels or image regions. While regionlevel models often feature dense pairwise connectivity, pixel-level models are considerably larger and have only permitted sparse graph structures. In this paper, we consider fully connected CRF models defined on the complete set of pixels in an image. The resulting graphs have billions of edges, making traditional inference algorithms impractical. Our main contribution is a highly efficient approximate inference algorithm for fully connected CRF models in which the pairwise edge potentials are defined by a linear combination of Gaussian kernels. Our experiments demonstrate that dense connectivity at the pixel level substantially improves segmentation and labeling accuracy.},
	archivePrefix = {arXiv},
	arxivId = {arXiv:1210.5644v1},
	author = {Kr{\"{a}}henb{\"{u}}hl, Philipp and Koltun, Vladlen},
	eprint = {arXiv:1210.5644v1},
	file = {:home/minakram96/Desktop/BachelorThesis/literature/2012- crf.pdf:pdf},
	isbn = {9781618395993},
	journal = {Advances in Neural Information Processing Systems 24: 25th Annual Conference on Neural Information Processing Systems 2011, NIPS 2011},
	pages = {1--9},
	title = {{Efficient inference in fully connected crfs with Gaussian edge potentials}},
	year = {2011}
}


@incollection{Burgard.2016,
 author = {Burgard, Wolfram and Hebert, Martial and Bennewitz, Maren},
 title = {World Modeling},
 url = {\url{https://doi.org/10.1007/978-3-319-32552-1_45}},
 pages = {1135--1152},
 publisher = {{Springer International Publishing}},
 isbn = {978-3-319-32552-1},
 editor = {Siciliano, Bruno and Khatib, Oussama},
 booktitle = {Springer Handbook of Robotics},
 year = {2016},
 address = {Cham},
 doi = {\url{10.1007/978-3-319-32552-1{\textunderscore }45}}
}


